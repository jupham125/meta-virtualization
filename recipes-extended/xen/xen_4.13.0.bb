FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
require xen.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=4295d895d4b5ce9d070263d52f030e49"

SRC_URI = " \
    https://downloads.xenproject.org/release/xen/${PV}/xen-${PV}.tar.gz \
    file://0001-python-pygrub-pass-DISTUTILS-xen.4.13.patch \
    "

SRC_URI[md5sum] = "d3b13c4c785601be2f104eaddd7c6a00"
SRC_URI[sha256sum] = "c69ae21b2ddeaf25532a81a448fcc6a218bc56f93c8907b2d416b2d4339c0afe"

S = "${WORKDIR}/xen-${PV}"
